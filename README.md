# Evaluating Performance Characteristics of Opportunistic Routing Protocols in ONE

Course Work of the module DACN, Theme: Evaluating Performance Characteristics of Opportunistic Routing Protocols in ONE

<object data="https://gitlab.com/Alexander-Koch-Regensburg/evaluating-performance-characteristics-of-opportunistic-routing-protocols-in-one/-/blob/master/report.pdf" type="application/pdf" width="700px" height="700px">
    <embed src="report.pdf">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="report.pdf">Download PDF</a>.</p>
    </embed>
</object>

Copyright 2020 Alexander Koch